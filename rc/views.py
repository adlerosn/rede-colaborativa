from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from datetime import datetime, timedelta
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.views.decorators.cache import cache_page

from .forms import FormularioColaboracao, FormularioCampus

from .models import Campus, Curso, CEP, Endereco, Colaborador, Colaboracao, Competencia, Evento, Participacao, NivelDeProficiencia, RecuperacaoDeSenha, Contato

# Create your views here.

def getLoggedUser():
    try:
        return Colaborador.objects.get(pk=1) #simula um usuário logado no sistema, a ser substituído por um código funcional
    except:
        return None

usuarioLogado = getLoggedUser()
#usuarioLogado = None

@cache_page(60 * 9999)
def bootstrap(request,file):
    c=''
    f = open('rc/static/bootstrap/'+file)
    c = f.read()
    f.close()
    ctyped = {
        None:'text/html',
        'css':'text/css',
        'js':'text/javascript',
    }
    ctype = ctyped[None]
    try:
        ctype = ctyped[file.split('.')[-1]]
    except:
        pass
    response = HttpResponse(c,content_type=ctype)
    return response

def home(request):
    template = loader.get_template('page_main.html')
    context = {
        'titulodapagina': 'Rede colaborativa',
        'body':'startup.html',
        'visitante':usuarioLogado,
        'sidebar_links':[{'url':'home',
                          'active':True,
                          'label':'Home',
                         },
                         {'url':'part_index',
                          'active':False,
                          'label':'Colaborações',
                         },
                         {'url':'adm_cam_listar',
                          'active':False,
                          'label':'Campi',
                         },
#                         {'url':'',
#                          'active':False,
#                          'label':'fgrad',
#                         },
                        ],
    }
    return HttpResponse(template.render(context, request))

def participacoes_index(request):
    time_threshold = datetime.now() + timedelta(hours=24)
    latest_colaboration_list = (Colaboracao.objects.
        filter(inicio__gt=time_threshold).
        order_by('inicio'))
    context = {
        'titulodapagina': 'Colaborações | Rede colaborativa',
        'tituloh3':'Colaborações',
        'toShow_list': latest_colaboration_list,
        'linkParaSair': False,
        'new_url':'part_criar',
        'new_label':'Oferecer',
        'brief_info_part':'participacoes/listar_dl.html',
    }
    template = loader.get_template('page_main.html')
    context2 = {
        'body':'crud/listar.html',
        'visitante':usuarioLogado,
        'sidebar_links':[{'url':'home',
                          'active':False,
                          'label':'Home',
                         },
                         {'url':'part_index',
                          'active':True,
                          'label':'Colaborações',
                         },
                         {'url':'adm_cam_listar',
                          'active':False,
                          'label':'Campi',
                         },
#                         {'url':'',
#                          'active':False,
#                          'label':'fgrad',
#                         },
                        ],
    }
    context2.update(context)
    return HttpResponse(template.render(context2, request))

def participacoes_individuais(request):
    time_threshold = datetime.now() + timedelta(hours=24)
    latest_colaboration_list = (Colaboracao.objects.
        filter(inicio__gt=time_threshold).
        filter(criador=usuarioLogado).
        order_by('inicio'))
    context = {
        'titulodapagina': 'Minhas Colaborações | Rede colaborativa',
        'tituloh3':'Colaborações: Minhas inscrições',
        'toShow_list': latest_colaboration_list,
        'linkParaSair': True,
        'new_url':'part_criar',
        'new_label':'Oferecer',
        'brief_info_part':'participacoes/listar_dl.html',
    }
    template = loader.get_template('page_main.html')
    context2 = {
        'body':'crud/listar.html',
        'visitante':usuarioLogado,
        'sidebar_links':[{'url':'home',
                          'active':False,
                          'label':'Home',
                         },
                         {'url':'part_index',
                          'active':True,
                          'label':'Colaborações',
                         },
                         {'url':'adm_cam_listar',
                          'active':False,
                          'label':'Campi',
                         },
#                         {'url':'',
#                          'active':False,
#                          'label':'fgrad',
#                         },
                        ],
    }
    context2.update(context)
    return HttpResponse(template.render(context2, request))

def participacoes_visualizar(request, part_id):
    col = Colaboracao.objects.get(pk=int(part_id))
    participacoes = Participacao.objects.filter(colaboracao=int(part_id))
    cp = len(participacoes)
    inscrito = len(participacoes.filter(colaborador=usuarioLogado))>0
    context = {
        'titulodapagina': 'Participacao | Rede colaborativa',
        'tituloh3':'Detalhes da participação',
        'colaboracao': col,
        'contagemParticipantes': cp,
        'linkParaSair': inscrito,
        'template_detailed_list':'participacoes/detalhar_dl.html',
        'template_detailed_actions':'participacoes/detalhar_da.html',
    }
    template = loader.get_template('page_main.html')
    context2 = {
        'body':'crud/detalhar.html',
        'visitante':usuarioLogado,
        'sidebar_links':[{'url':'home',
                          'active':False,
                          'label':'Home',
                         },
                         {'url':'part_index',
                          'active':True,
                          'label':'Colaborações',
                         },
                         {'url':'adm_cam_listar',
                          'active':False,
                          'label':'Campi',
                         },
#                         {'url':'',
#                          'active':False,
#                          'label':'fgrad',
#                         },
                        ],
    }
    context2.update(context)
    return HttpResponse(template.render(context2, request))

def participacoes_inscrever(request, part_id):
    colaboracao = Colaboracao.objects.get(pk=int(part_id))
    novaParticipacao = Participacao(colaborador=usuarioLogado,colaboracao=colaboracao)
    novaParticipacao.save()
    return HttpResponseRedirect(reverse('part_listar', current_app=request.resolver_match.namespace))

def participacoes_desinscrever(request, part_id):
    colaboracao = Colaboracao.objects.get(pk=int(part_id))
    apagarColaboracao = colaboracao.criador.pk==usuarioLogado.pk
    if apagarColaboracao:
        colaboracao.delete()
    else:
        Participacao.objects.get(colaborador=usuarioLogado,colaboracao=colaboracao).delete()
    return HttpResponseRedirect(reverse('part_listar', current_app=request.resolver_match.namespace))

def participacoes_criar(request):
    form = FormularioColaboracao()
    erro = None
    if request.method=='POST':
        form = FormularioColaboracao(request.POST)
        if form.is_valid():
            fd = form.cleaned_data
            c = Colaboracao(titulo=fd['titulo'],
                            objetivos=fd['objetivos'],
                            inicio=fd['inicio'],
                            fim=fd['fim'],
                            maxpart=fd['maximoParticipantes'],
                            certificacao=fd['certificacao'],
                            local=fd['local'],
                            criador=usuarioLogado)
            c.save()
            return participacoes_inscrever(request, str(c.pk))
        else:
            erro = 'Erro de validação'
    context = {
        'titulodapagina': 'Criar Colaboração | Rede colaborativa',
        'tituloh3':'Colaborações: Oferecer',
        'submit_url_pattern':'part_criar',
        'back_url_pattern':'part_listar',
        'form': form,
        'erro': erro,
        'sumbmit_label':'Oferecer',
        'back_label':'Cancelar',
    }
    template = loader.get_template('page_main.html')
    context2 = {
        'body':'crud/criar.html',
        'visitante':usuarioLogado,
        'sidebar_links':[{'url':'home',
                          'active':False,
                          'label':'Home',
                         },
                         {'url':'part_index',
                          'active':True,
                          'label':'Colaborações',
                         },
                         {'url':'adm_cam_listar',
                          'active':False,
                          'label':'Campi',
                         },
#                         {'url':'',
#                          'active':False,
#                          'label':'fgrad',
#                         },
                        ],
    }
    context2.update(context)
    return HttpResponse(template.render(context2, request))

def campus_inserir(request):
    form = FormularioCampus()
    erro = None
    if request.method=='POST':
        form = FormularioCampus(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            c = Campus(nome=cd['nome'],exibir=cd['exibir'])
            c.save()
            return HttpResponseRedirect(reverse('adm_cam_listar', current_app=request.resolver_match.namespace))
        else:
            erro = 'Erro de validação'
    context = {
        'titulodapagina': 'Criar Campus | Rede colaborativa',
        'tituloh3': 'Cadastrar novo campus',
        'form': form,
        'erro': erro,
        'submit_url_pattern':'adm_cam_inserir',
        'back_url_pattern':'adm_cam_listar',
        'sumbmit_label':'Cadastrar',
        'back_label':'Cancelar',
    }
    template = loader.get_template('page_main.html')
    context2 = {
        'body':'crud/criar.html',
        'visitante':usuarioLogado,
        'sidebar_links':[{'url':'home',
                          'active':False,
                          'label':'Home',
                         },
                         {'url':'part_index',
                          'active':False,
                          'label':'Colaborações',
                         },
                         {'url':'adm_cam_listar',
                          'active':True,
                          'label':'Campi',
                         },
#                         {'url':'',
#                          'active':False,
#                          'label':'fgrad',
#                         },
                        ],
    }
    context2.update(context)
    return HttpResponse(template.render(context2, request))

def campus_alterar(request,pk):
    campus = Campus.objects.get(pk=int(pk))
    form = FormularioCampus(initial={'pk':campus.id,'nome':campus.nome,'exibir':campus.exibir})
    erro = None
    if request.method=='POST':
        form = FormularioCampus(request.POST)
        if form.is_valid():
            cd = form.cleaned_data
            c = Campus(pk=cd['pk'],nome=cd['nome'],exibir=cd['exibir'])
            c.save()
        else:
            erro = 'Erro de validação'
    context = {
        'titulodapagina': 'Editar Campus | Rede colaborativa',
        'form': form,
        'erro': erro,
        'tituloh3':'Editar campus',
        'excluir_habilitado':True,
        'submit_url':'adm_cam_alterar',
        'del_url':'adm_cam_excluir',
        'back_url':'adm_cam_listar',
        'submit_label':'Alterar',
        'del_label':'Excluir',
        'back_label':'Voltar',
        'item': campus,
    }
    template = loader.get_template('page_main.html')
    context2 = {
        'body':'crud/editar.html',
        'visitante':usuarioLogado,
        'sidebar_links':[{'url':'home',
                          'active':False,
                          'label':'Home',
                         },
                         {'url':'part_index',
                          'active':False,
                          'label':'Colaborações',
                         },
                         {'url':'adm_cam_listar',
                          'active':True,
                          'label':'Campi',
                         },
#                         {'url':'',
#                          'active':False,
#                          'label':'fgrad',
#                         },
                        ],
    }
    context2.update(context)
    return HttpResponse(template.render(context2, request))

def campus_listar(request):
    campi = Campus.objects.all()
    context = {
        'titulodapagina': 'Campi | Rede colaborativa',
        'tituloh3':'Campi',
        'toShow_list': campi,
        'new_url':'adm_cam_inserir',
        'new_label':'Cadastrar novo campus',
        'detail_url':'adm_cam_alterar',
        'detail_label':'Editar',
        'brief_info_part':'administracao/campi/listar_dd.html',
    }
    template = loader.get_template('page_main.html')
    context2 = {
        'body':'crud/listar.html',
        'visitante':usuarioLogado,
        'sidebar_links':[{'url':'home',
                          'active':False,
                          'label':'Home',
                         },
                         {'url':'part_index',
                          'active':False,
                          'label':'Colaborações',
                         },
                         {'url':'adm_cam_listar',
                          'active':True,
                          'label':'Campi',
                         },
#                         {'url':'',
#                          'active':False,
#                          'label':'fgrad',
#                         },
                        ],
    }
    context2.update(context)
    return HttpResponse(template.render(context2, request))

def campus_excluir(request,pk):
    campus = Campus.objects.get(pk=int(pk))
    campus.delete()
    return HttpResponseRedirect(reverse('adm_cam_listar', current_app=request.resolver_match.namespace))
