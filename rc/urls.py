from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^bootstrap/(?P<file>.+)$', views.bootstrap, name='bootstrap'),
    url(r'^participacoes/?$', views.participacoes_index, name='part_index'),
    url(r'^participacoes/listarMinhas/?$', views.participacoes_individuais, name='part_listar'),
    url(r'^participacoes/visualizar/(?P<part_id>[0-9]+)/?$', views.participacoes_visualizar, name='part_detalhe'),
    url(r'^participacoes/inscrever/(?P<part_id>[0-9]+)/?$', views.participacoes_inscrever, name='part_inscrever'),
    url(r'^participacoes/desinscrever/(?P<part_id>[0-9]+)/?$', views.participacoes_desinscrever, name='part_desinscrever'),
    url(r'^participacoes/criar/?$', views.participacoes_criar, name='part_criar'),
    url(r'^administrador/campi/?$', views.campus_listar, name='adm_cam_listar'),
    url(r'^administrador/campi/inserir/?$', views.campus_inserir, name='adm_cam_inserir'),
    url(r'^administrador/campi/alterar/(?P<pk>[0-9]+)/?$', views.campus_alterar, name='adm_cam_alterar'),
    url(r'^administrador/campi/excluir/(?P<pk>[0-9]+)/?$', views.campus_excluir, name='adm_cam_excluir'),
]
