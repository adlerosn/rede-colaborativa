from django.db import models

# Create your models here.

class Campus(models.Model):
    """Class Campus
    """
    # Attributes:
    nome = models.CharField(max_length=70)  # (String) 
    exibir = models.BooleanField()  # (boolean) 
    
    # Operations
    def __str__(self):
        return 'Campus: '+str(self.nome)


class Curso(models.Model):
    """Class Curso
    """
    # Attributes:
    nome = models.CharField(max_length=70)  # (String) 
    exibir = models.BooleanField()  # (boolean) 
    
    # Operations
    def __str__(self):
        return 'Cusro: '+str(self.nome)


class CEP(models.Model):
    cep = models.CharField(max_length=8,db_index=True) # (String) 
    rua = models.CharField(max_length=70)  # (String) 
    bairro = models.CharField(max_length=70)  # (String) 
    cidade = models.CharField(max_length=70)  # (String) 
    estado = models.CharField(max_length=70)  # (String) 
    def __str__(self):
        return 'CEP: '+str(self.cep)

class Endereco(models.Model):
    numero = models.CharField(max_length=70)  # (int) 
    complemento = models.CharField(max_length=70)  # (String) 
    cep = models.ForeignKey(CEP)  # (String) 
    def __str__(self):
        return 'Endereco: '+str(self.cep)+', '+str(self.numero)

class Colaborador(models.Model):
    """Class Colaborador
    """
    # Attributes:
    nome = models.CharField(max_length=150)  # (String) 
    senha = models.CharField(max_length=255)  # (String) 
    email = models.CharField(max_length=70,db_index=True)  # (String) 
    administrador = models.BooleanField()  # (boolean) 
    endereco = models.ForeignKey(Endereco, null=True, blank=True) # (Endereco)
    campus = models.ForeignKey(Campus, null=True, blank=True)  # (Campus) 
    curso = models.ForeignKey(Curso, null=True, blank=True)  # (Curso) 
    
    # Operations
    def __str__(self):
        return 'Colaborador: '+str(self.nome)
    @classmethod
    def obterRecomendacoes(self):
        """function obterRecomendacoes
        
        returns ArrayList<Colaboracao>
        """
        return None # should raise NotImplementedError()
    @classmethod
    def podeBanir(self, outrem):
        """function podeBanir
        
        outrem: Colaborador
        
        returns boolean
        """
        return None # should raise NotImplementedError()

class Competencia(models.Model):
    """Class Competencia
    """
    # Attributes:
    nome = models.CharField(max_length=70)  # (string) 
    descricao = models.TextField()  # (string) 
    
    # Operations
    def __str__(self):
        return 'Competencia: '+str(self.nome)


class Colaboracao(models.Model):
    """Class Colaboracao
    """
    # Attributes:
    titulo = models.CharField(max_length=30,default="")  # (string) 
    objetivos = models.CharField(max_length=255)  # (string) 
    inicio = models.DateTimeField()  # (Date) 
    fim = models.DateTimeField()  # (Date) 
    maxpart = models.IntegerField()  # (int) 
    certificacao = models.BooleanField()  # (boolean) 
    local = models.CharField(max_length=255)
    criador = models.ForeignKey(Colaborador, null=False, blank=False, default=1,db_index=True)
    competencia = models.ForeignKey(Competencia, null=True, blank=False, db_index=True)
    
    
    # Operations
    def __str__(self):
        return 'Colaboracao: '+str(self.titulo)
    @classmethod
    def apagavel(self, colr, agora):
        """function apagavel
        
        colr: Colaborador
        agora: Date
        
        returns boolean
        """
        return None # should raise NotImplementedError()


class Evento(models.Model):
    """Class Evento
    """
    # Attributes:
    dataEHora = models.DateTimeField(auto_now_add=True)  # (Date) 
    colaboracao = models.ForeignKey(Colaboracao,related_name="eventos",db_index=True)  # (Colaboracao) 
    competencia = models.ForeignKey(Competencia,db_index=True)  # (Competencia) 
    
    # Operations
    def __str__(self):
        return 'Evento: '+str(self.id)+', '+str(self.colaboracao.titulo)
    @classmethod
    def apagavel(self, colr, agora):
        """function apagavel
        
        colr: Colaborador
        agora: Date
        
        returns boolean
        """
        return None # should raise NotImplementedError()


class Participacao(models.Model):
    """Class Participacao
    """
    # Attributes:
    colaboracao = models.ForeignKey(Colaboracao,db_index=True) 
    colaborador = models.ForeignKey(Colaborador, related_name="participacoes",db_index=True)
    def __str__(self):
        return 'Participacao: '+str(self.colaborador.nome)+' <--> '+str(self.colaboracao.titulo)

    # Operations


class NivelDeProficiencia(models.Model):
    """Class NivelDeProficiencia
    """
    # Attributes:
    estrelas = models.IntegerField()  # (int) 
    descricao = models.TextField()  # (string) 
    querEnsinar = models.BooleanField() # (boolean) 
    desejaAprender = models.BooleanField()  # (boolean) 
    competencia = models.ForeignKey(Competencia,db_index=True)  # (Competencia) 
    colaborador = models.ForeignKey(Colaborador,related_name="niveis",db_index=True)  # (Colaborador) 
    
    # Operations
    def __str__(self):
        return 'NivelDeProficiencia: '+str(self.colaborador.nome)+' <--> '+str(self.competencia.nome)


class RecuperacaoDeSenha(models.Model):
    """Class RecuperacaoDeSenha
    """
    # Attributes:
    codigo = models.CharField(max_length=50)  # (string) 
    expira = models.DateTimeField(auto_now_add=True)  # (int) 
    colaborador = models.ForeignKey(Colaborador,db_index=True)  # (Colaborador) 
    
    # Operations
    def __str__(self):
        return 'RecuperacaoDeSenha: '+str(self.colaborador.nome)
    @classmethod
    def aindaValido(self):
        """function aindaValido
        
        returns boolean
        """
        return None # should raise NotImplementedError()

class Contato(models.Model):
    """Class Contato
    """
    # Attributes:
    colaborador = models.ForeignKey(Colaborador,related_name="contatos",db_index=True)  # (Colaborador) 
    tipocontato = models.CharField(max_length=70)  # (String) 
    contato = models.CharField(max_length=70)  # (string) 
    
    # Operations
    def __str__(self):
        return 'Contato: '+str(self.colaborador.nome)+' '+str(self.tipocontato)+' '+str(self.contato)


