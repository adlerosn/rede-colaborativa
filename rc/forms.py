from django import forms

class FormularioColaboracao(forms.Form):
    titulo = forms.CharField(label='Título', required=True, max_length=30)
    objetivos = forms.CharField(label='Objetivos', required=True, max_length=255)
    inicio = forms.DateTimeField(label='Inicio', required=True, widget=forms.DateTimeInput)
    fim = forms.DateTimeField(label='Fim', required=True, widget=forms.DateTimeInput)
    maximoParticipantes = forms.IntegerField(label='Máximo de participantes', required=False)
    certificacao = forms.BooleanField(label='Certificado', required=False)
    local = forms.CharField(label='Local', required=True, max_length=255)

class FormularioCampus(forms.Form):
    pk = forms.IntegerField(widget=forms.HiddenInput,initial=-1)
    nome = forms.CharField(label='Nome', required=True, max_length=30)
    exibir = forms.BooleanField(label='Exibir', required=False, initial=True)

