from django.test import TestCase
from .models import Campus, Curso, CEP, Endereco, Colaborador, Colaboracao, Competencia, Evento, Participacao, NivelDeProficiencia, RecuperacaoDeSenha, Contato

# Create your tests here.
class CampusTestCase(TestCase):
    def setUp(self):
        Campus.objects.create(nome="IFES Colatina", exibir=True)
        Campus.objects.create(nome="IFES Iconha", exibir=False)
        Campus.objects.create(nome="UFES Goiabeiras", exibir=True)
    def test_campus_insercao(self):
        Campus.objects.create(nome="UFES Alegre", exibir=False)
    def test_campus_selecao(self):
        Campus.objects.get(exibir=False)
    def test_campus_alterar(self):
        for campus in Campus.objects.filter(exibir=False):
            campus.exibir = True
            campus.save()
    def test_campus_apagar(self):
        Campus.objects.get(nome="UFES Goiabeiras").delete()
#    def test_campus_alterar_certo(self):
#        campus = Campus.objects.get(nome="IFES Iconha")
#        campus.nome = "IFES Linhares"
#        campus.save()
#        campi = Campus.objects.filter(nome="IFES Linhares")
#        self.assertEqual(len(campi)>0,True)
    def test_campus_alterar_bugado(self):
        campus = Campus.objects.get(nome="IFES Iconha")
        campus.nome = "IFES Linhares"
        #campus.save()
        campi = Campus.objects.filter(nome="IFES Linhares")
        self.assertEqual(len(campi)>0,True)

