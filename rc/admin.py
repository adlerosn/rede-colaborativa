from django.contrib import admin

from .models import *

# Register your models here.

admin.site.register(Campus)
admin.site.register(Curso)
admin.site.register(CEP)
admin.site.register(Endereco)
admin.site.register(Colaborador)
admin.site.register(Colaboracao)
admin.site.register(Competencia)
admin.site.register(Evento)
admin.site.register(Participacao)
